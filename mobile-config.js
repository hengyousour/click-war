// Set up resources such as icons and launch screens.
App.info({
    name: 'Click War',
    description: 'War Begin',
    version: '0.0.1'
});

// App.icons({
//     //android
//     'android_mdpi':'resources/icons/drawable-mdpi/icon.png',
//     'android_hdpi':'resources/icons/drawable-hdpi/icon.png',
//     'android_xhdpi': 'resources/icons/drawable-xhdpi/icon.png',
//     'android_xxhdpi':'resources/icons/drawable-xxhdpi/icon.png',
//     'android_xxxhdpi':'resources/icons/drawable-xxxhdpi/icon.png'
// });
//
// App.launchScreens({
//     //android
//     'android_mdpi_portrait':'resources/splashs/drawable-mdpi/screen.png',
//     'android_mdpi_landscape':'resources/splashs/drawable-land-mdpi/screen.png',
//     'android_hdpi_portrait':'resources/splashs/drawable-hdpi/screen.png',
//     'android_hdpi_landscape':'resources/splashs/drawable-land-hdpi/screen.png',
//     'android_xhdpi_portrait': 'resources/splashs/drawable-xhdpi/screen.png',
//     'android_xhdpi_landscape': 'resources/splashs/drawable-land-xhdpi/screen.png',
//     'android_xxhdpi_portrait':'resources/splashs/drawable-xxhdpi/screen.png',
//     'android_xxhdpi_landscape':'resources/splashs/drawable-land-xxhdpi/screen.png'
// });
App.accessRule('*',{type:'navigation'}); //inapp browser