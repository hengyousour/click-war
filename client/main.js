import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import Vue from 'vue';
import Vuetify from 'vuetify/dist/vuetify.min.js';
import 'vuetify/dist/vuetify.min.css';
import VueRouter from 'vue-router';
import VueSweetAlert from 'vue-sweetalert';

import Layout from '/imports/ui/container/Layout.vue';
import Home from '/imports/ui/container/Home.vue';
import Game from '/imports/ui/container/Game.vue';


Vue.use(VueSweetAlert)
Vue.use(Vuetify);
Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {path: '/', name:'home', component: Home},
        {path:'/game/:p1/:t1/:p2/:t2/:click',name:'game' , component: Game}
    ]
});

Meteor.startup(function () {
    new Vue({
        router,
        render: h => h(Layout),
        // components: AppLayout,
    }).$mount('app');
});